package com.klymchuk.knife;

public class Blade {
    private int length;
    private int width;

    public Blade(int length, int width) {
        this.length = length;
        this.width = width;
    }

    public Blade() {
        this.length = 0;
        this.width = 0;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public String toString() {
        return "Blade{" +
                "length=" + length +
                ", width=" + width +
                '}';
    }
}
