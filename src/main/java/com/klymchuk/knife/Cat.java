package com.klymchuk.knife;

public class Cat {
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "count=" + count +
                '}';
    }
}
