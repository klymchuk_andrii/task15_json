package com.klymchuk;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.klymchuk.jacksone.Jackson;

import java.io.File;
import java.io.IOException;

import com.klymchuk.validator.MyValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    public static void main(String[] args) {
        Logger logger = LogManager.getLogger(Application.class);
        File json = new File("C:\\Users\\Andrii\\IdeaProjects\\task15\\src\\main\\resources\\knifes.json");
        File jsonSchema = new File("C:\\Users\\Andrii\\IdeaProjects\\task15\\src\\main\\resources\\knifeSchem.json");

        try{
            if(MyValidator.validate(json,jsonSchema)){
                logger.info("Good");
            }else {
                logger.info("Bad");
            }
        } catch (IOException | ProcessingException e) {
            e.printStackTrace();
        }
        Jackson jackson = new Jackson();

        logger.info(jackson.getKnifeList(json));
    }
}
