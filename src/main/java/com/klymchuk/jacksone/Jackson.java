package com.klymchuk.jacksone;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.klymchuk.knife.Dog;
import com.klymchuk.knife.Knife;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class Jackson {
    private ObjectMapper mapper;

    public Jackson() {
        mapper = new ObjectMapper();
    }

    public List<Knife> getKnifeList(File json) {
        Knife[] knifes = new Knife[0];
        Dog dog = new Dog();
        try {
            dog = mapper.readValue(new File("src/main/resources/dog.json"),Dog.class);
            System.out.println(dog);
            knifes = mapper.readValue(json, Knife[].class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Arrays.asList(knifes);
    }
}
